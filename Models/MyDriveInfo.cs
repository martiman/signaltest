﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SignalTest.Models
{
    public class MyDriveInfo
    {
        public string Name { get; set; }
        public long Total { get; set; }
        public long Free { get; set; }
    }
}