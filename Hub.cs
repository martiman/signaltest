﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.IO;
using System.Diagnostics;
using SignalTest.Models;
using Newtonsoft.Json;
using System.Text;

namespace SignalTest
{
    public class TestHub : Hub
    {
        List<MyDriveInfo> allDrives = new List<MyDriveInfo>();

        public TestHub()
        {
            foreach (var di in DriveInfo.GetDrives().Where(x=>x.DriveType == DriveType.Fixed))
            {            
                WatchDrive(di.Name);
            }                
        }

        public void WatchDrive(string path)
        {
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = path;
            watcher.NotifyFilter = NotifyFilters.Size | NotifyFilters.LastWrite;
            watcher.IncludeSubdirectories = true;
            watcher.InternalBufferSize = 65536;
            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.EnableRaisingEvents = true;
        }

        public void RefreshDriveInfo()
        {
            var dis = DriveInfo.GetDrives();
            foreach( var di in dis )
            {
                if (di.DriveType == DriveType.Fixed)
                {
                    var d = allDrives.FirstOrDefault(x => x.Name == di.Name);
                    if (d != null)
                    {
                        d.Free = di.AvailableFreeSpace;
                    }
                    else
                    {
                        var newDrive = new MyDriveInfo()
                        {
                            Name = di.Name,
                            Total = di.TotalSize,
                            Free = di.AvailableFreeSpace
                        };

                        allDrives.Add(newDrive);
                    }
                }
            }
        }

        public void ShowDriveInfo()
        {
            Clients.All.empty();
            foreach (var d in allDrives)
            {
                Clients.All.showDriveInfo(d.Name, d.Total, d.Free);
            }
        }

        private void OnChanged(object source, FileSystemEventArgs e)
        {
            RefreshDriveInfo();
            ShowDriveInfo();
        }
    }
}