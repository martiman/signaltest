﻿using Microsoft.Owin;
using Owin;
using System.IO;
using Microsoft.AspNet.SignalR;

[assembly: OwinStartup(typeof(SignalTest.Startup))]
namespace SignalTest
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}